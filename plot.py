#!/usr/bin/env python
import pickle
import matplotlib.pyplot as plt
import math
import matplotlib
import numpy as np

with open("pickletotout", "rb") as pfile:
	finished_replays, total = pickle.load(pfile)

races_long = {'Z':'Zerg', 'T': 'Terran', 'P': 'Protoss'}

for race in ['Z', 'T', 'P']:

	upgrades = set([])
	for enemy in ['Z', 'T', 'P']:
		for el in total[race+'v'+enemy]:
			upgrades.update(set(el['upgrades']))

	# ignored_upgrades = [
	# 	"ZergMissileWeaponsLevel1",
	# 	"ZergMissileWeaponsLevel2", 
	# 	"ZergMissileWeaponsLevel3",
	# 	"ZergMeleeWeaponsLevel1",
	# 	"ZergMeleeWeaponsLevel2",
	# 	"ZergMeleeWeaponsLevel3",
	# 	"ZergGroundArmorsLevel1",
	# 	"ZergGroundArmorsLevel2",
	# 	"ZergGroundArmorsLevel3",
	# 	"ZergFlyerWeaponsLevel1",
	# 	"ZergFlyerWeaponsLevel2",
	# 	"ZergFlyerWeaponsLevel3",
	# 	"ZergFlyerArmorsLevel1",
	# 	"ZergFlyerArmorsLevel2",
	# 	"ZergFlyerArmorsLevel3",
	# 	]

	# upgrades = ["zerglingmovementspeed", "Burrow", "TunnelingClaws", "CentrificalHooks", "EvolveMuscularAugments", "DiggingClaws", "overlordspeed", "zerglingattackspeed", "GlialReconstitution", "LurkerRange", "InfestorEnergyUpgrade", "EvolveGroovedSpines", "ChitinousPlating", "NeuralParasite", "AnabolicSynthesis"]

	events = {"active": []}
	for upgrade in upgrades:
		events[upgrade] = []

	for enemy in ['Z', 'T', 'P']:
		for el in total[race+'v'+enemy]:
			# if el["endframe"] == 32836: #TODO: remove this ugly hack
			# 	continue
			events["active"].append((0, +1))
			events["active"].append((el["endframe"], -1))
			for upgrade in el['upgrades']:
				assert upgrade in upgrades or upgrade in ignored_upgrades, str(upgrade)
				if upgrade in upgrades:
					val = el['upgrades'][upgrade]
					events[upgrade].append((val, +1))
					events[upgrade].append((el['endframe'], -1))

	significant_frames = set()
	for key, value in events.items():
		for (frame, diff) in value:
			significant_frames.add(frame)

	def count_upto(what, frame):
		count = 0
		for (time,diff) in events[what]:
			if time <= frame:
				count += diff
		return count

	def count_positive(what, frame):
		count = 0
		for (time,diff) in events[what]:
			if time <= frame:
				if diff > 0:
					count += diff
		return count

	peakdict = dict([(upgrade, count_positive(upgrade, max(significant_frames))) for upgrade in upgrades])
	#print(sorted(peakdict, key=peakdict.get)[-6:])
	upgrades = sorted(peakdict, key=peakdict.get)[-10:]

	plotdat = {"active": {}}
	for upgrade in upgrades:
		plotdat[upgrade] = {}

	for frame in significant_frames:
		# if frame != 0:
		# 	plotdat["active"][frame-1] = count_upto("active", frame-1)/len(total['ZvZ'])
		# plotdat["active"][frame] = count_upto("active", frame)/len(total['ZvZ'])

		for upgrade in upgrades:
			if (count_upto("active", frame)) == 0:
				assert count_upto(upgrade, frame) == 0
				continue
			plotdat[upgrade][frame] = count_upto(upgrade, frame)/(count_upto("active", frame))	
			if (count_upto("active", frame-1)) == 0:
				assert count_upto(upgrade, frame-1) == 0
				continue
			if frame != 0:
				plotdat[upgrade][frame-1] = count_upto(upgrade, frame-1)/(count_upto("active", frame-1))

	plt.figure(figsize=(10,7))
	for label, line in plotdat.items():
		time_plotdat = {}
		for key, value in line.items():
			time_plotdat[key/16/60/60/24] = value
		plt.plot(*zip(*sorted(time_plotdat.items())), label=label)
	plt.legend()

	import matplotlib.dates as mdates
	locator = mdates.AutoDateLocator(minticks=6, maxticks=20)
	plt.gca().get_xaxis().set_major_formatter(mdates.DateFormatter("%M:%S"))
	plt.gca().get_xaxis().set_major_locator(locator)

	plt.xlim(0.9*np.sort(list(significant_frames))[1]/16/60/60/24, 1.01*np.sort(list(significant_frames))[-2]/16/60/60/24)
	plt.xlim(4/60/24, 30/60/24)
	plt.ylabel("Number of games with upgrade / Number of games active")
	plt.title("Top 10 " + races_long[race] + " upgrades")
	plt.savefig(races_long[race])
	#plt.show()
