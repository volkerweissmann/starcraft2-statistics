#!/usr/bin/env python
import sc2reader
import os
import zipfile
import tempfile
import shutil
import time
import pickle
import hashlib

_startTimeMillis = int(round(time.time() * 1000))
def millis():
	return int(round(time.time() * 1000))-_startTimeMillis


def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

class MyPlugin():
	name = "MyPlugin"
	def handleEvent(self, event, replay):
		try:
			if not hasattr(replay, 'hacky_output'):
				replay.hacky_output = ({'upgrades':{}, 'done':[], 'died':[], 'endframe':None},{'upgrades':{}, 'done':[], 'died':[], 'endframe':None})
				assert hasattr(replay, 'hacky_output')
			if isinstance(event, sc2reader.events.tracker.PlayerSetupEvent):
				pass
			elif isinstance(event, sc2reader.events.tracker.UpgradeCompleteEvent):
				assert event.count == 1
				if event.upgrade_type_name in ['GameHeartActive', 'SprayZerg', 'SprayTerran', 'SprayProtoss'] or 'Dance' in event.upgrade_type_name:
					return
				assert event.upgrade_type_name not in replay.hacky_output[event.pid-1]
				replay.hacky_output[event.pid-1]['upgrades'][event.upgrade_type_name] = event.frame
			elif isinstance(event, sc2reader.events.tracker.UnitDiedEvent): #morphing, merging, and getting killed.
				if event.unit.name in ['KarakMale', 'KarakFemale', 'Ursadon', 'CarrionBird'] or 'Destructible' in event.unit.name or 'MineralField' in event.unit.name or 'ForceField' in event.unit.name:
					return
				replay.hacky_output[event.unit.owner.pid-1]['died'].append((event.frame, event.unit.name))
			elif isinstance(event, sc2reader.events.tracker.UnitDoneEvent): #warp-in finished, building finished, morph complete.
				replay.hacky_output[event.unit.owner.pid-1]['done'].append((event.frame, event.unit.name))
			elif isinstance(event, sc2reader.events.game.PlayerLeaveEvent):
				if event.player.pid == 1 or event.player.pid == 2:
					if replay.hacky_output[0]['endframe'] is None:
						replay.hacky_output[0]['endframe'] = event.frame
						replay.hacky_output[1]['endframe'] = event.frame
			elif isinstance(event, sc2reader.events.tracker.UnitBornEvent):
				pass
			elif isinstance(event, sc2reader.events.message.ProgressEvent):
				pass
			elif isinstance(event, sc2reader.events.game.UserOptionsEvent):
				pass
			elif isinstance(event, sc2reader.events.tracker.PlayerStatsEvent):
				pass
			elif isinstance(event, sc2reader.events.game.DataCommandEvent): #Warp prism and bunker unloading
				pass
			elif isinstance(event, sc2reader.events.tracker.UnitTypeChangeEvent): # Unit morphing
				pass
			elif isinstance(event, sc2reader.events.game.BasicCommandEvent): # Alle befehle, inclusive starten von upgrades und bewegen von einheiten
				pass
			elif isinstance(event, (sc2reader.events.game.CameraEvent,
									sc2reader.events.game.TargetUnitCommandEvent,
									sc2reader.events.game.AddToControlGroupEvent,
									sc2reader.events.game.UpdateTargetPointCommandEvent,
									sc2reader.events.game.SetControlGroupEvent,
									sc2reader.events.game.ControlGroupEvent,
									sc2reader.events.game.TargetPointCommandEvent,
									sc2reader.events.tracker.UnitDiedEvent,
									sc2reader.events.tracker.UnitPositionsEvent,
									sc2reader.events.tracker.UnitInitEvent,
									sc2reader.events.message.ChatEvent,
									sc2reader.events.tracker.UnitOwnerChangeEvent,
									sc2reader.events.game.SelectionEvent,
									sc2reader.events.message.PingEvent,
									sc2reader.events.game.HijackReplayGameEvent)):
				pass
			else:
				raise ValueError("unknown event:" + str(type(event)) + str(event))
				#print(type(event), event)
				#breakpoint()

		except Exception as ex:
			import traceback
			traceback.print_exc()
			print('EXCEPTION OCCURED:', ex)

sc2reader.engine.register_plugin(MyPlugin())

if os.path.exists('pickletotout') and False:
	with open("pickletotout", "rb") as pfile:
		finished_replays, total = pickle.load(pfile)
else:
	finished_replays = []
	total = {}
	for r1 in ['Z', 'T', 'P']:
		for r2 in ['Z', 'T', 'P']:
			total[r1+'v'+r2] = []

def get_races(replay):
	races = [None, None]
	teams = list()
	for team in replay.teams:
		assert len(list(team)) == 1
		for player in team:
			race = player.pick_race
			race = race[0] # T for Terran, P for Protoss, Z for Zerg
			assert races[player.pid-1] is None
			races[player.pid-1] = race
	return races


def anal_file(path):
	print(path, flush=True)
	filehash = md5(path)
	if filehash in finished_replays:
		print("we already did that")
		return
	if path.split("/")[-1] in ["Time vs Silky G1.SC2Replay", "Time vs Silky G2.SC2Replay", "Time vs Silky G3.SC2Replay"]: #todo: was macht das hier
		pass

	try:
		replay = sc2reader.load_replay(path, load_map=False)
		#sc2reader.engine.run(replay)
		races = get_races(replay)
		out = replay.hacky_output
		normal = races[0] + 'v' + races[1]
		invert = races[1] + 'v' + races[0]
	except Exception as ex:
		print('failed to do it', flush=True)
		raise ex
		return
	try:
		total[normal].append(out[0])
		total[invert].append(out[1])
		finished_replays.append(filehash)
	except:
		import traceback
		traceback.print_exc()
		print('Exceptions in critical path, no saving possible')
		exit(1)

print("started", millis())
def read_replays_and_zips(rootdir):
	for root, subdirs, files in os.walk(rootdir):
		for el in files:
			fp = os.path.join(root, el)
			if el.endswith('.SC2Replay'):
				anal_file(fp)
			elif el.endswith('.zip'):
				dirpath = tempfile.mkdtemp()
				try:
					with zipfile.ZipFile(fp, 'r') as zip_ref:
						zip_ref.extractall(dirpath)
						read_replays_and_zips(dirpath)
				except KeyboardInterrupt as ex:
					print("----------------------------------------")
					shutil.rmtree(dirpath)
					raise ex
				except Exception as ex:
					shutil.rmtree(dirpath)
					raise ex

				else:
					shutil.rmtree(dirpath)

try:
	read_replays_and_zips("/home/volker/Downloads/2021_01-IEM-Katowice-2021")
except KeyboardInterrupt as ex:
	import traceback
	traceback.print_exc()
	print('exception occured')
except Exception as ex:
	import traceback
	traceback.print_exc()
	print('exception occured')

with open("pickletotout", "wb") as pfile:
	pickle.dump((finished_replays, total), pfile)